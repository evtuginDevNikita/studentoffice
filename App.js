import React, { Component } from 'react';
import { Font, AppLoading } from 'expo';
import { Ionicons } from '@expo/vector-icons';
import { Root } from 'native-base';

import Main from './src/Main'

export default class App extends Component {
  state = {
    loading: true
    }
    
  async componentWillMount() {
    await Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
      ...Ionicons.font
    });
    this.setState({ loading: false });
  }
  render() {
    const { loading } = this.state //деструктуризация

    if (loading) {
      return (
        <Root>
          <AppLoading />
        </Root>
      );
    };
    
    return(
      <Main />
    )
  };
}