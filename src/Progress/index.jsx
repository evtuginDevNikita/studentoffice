import React, { Component } from "react";
import { StyleSheet } from "react-native";
import Swiper from "react-native-swiper";
import { Container, Content, Text, Grid, Col, Row } from "native-base";
import Headers from "../common/Headers/";

const styles = StyleSheet.create({
  justifyContentCenter: {
    justifyContent: "center"
  },
  textPadding: {
    padding: 10
  },
  titleBorder: {
    borderBottomWidth: 1,
    marginHorizontal: 5,
    borderBottomColor: "grey"
  },
  contentCenter: {
    alignContent: "center",
    justifyContent: "center"
  }
});

export default class Progress extends Component {
  render() {
    return (
      <Container>
        <Headers
          onPress={() => this.props.navigation.toggleDrawer()}
          title="Успеваемость"
        />
        <Swiper>
          <Content>
            <Grid>
              <Row style={styles.titleBorder}>
                <Col size={60} style={styles.justifyContentCenter}>
                  <Text style={styles.textPadding}>Предмет</Text>
                </Col>
                <Col size={20} style={styles.justifyContentCenter}>
                  <Text style={styles.textPadding}>Зач/экз</Text>
                </Col>
                <Col size={20} style={styles.justifyContentCenter}>
                  <Text style={styles.textPadding}>Оценка</Text>
                </Col>
              </Row>
              <Row style={styles.titleBorder}>
                <Col size={60} style={styles.justifyContentCenter}>
                  <Text style={styles.textPadding}>
                    Технологии распределенных систем и параллельных исчеслений
                  </Text>
                </Col>
                <Col size={20} style={styles.justifyContentCenter}>
                  <Text style={styles.textPadding}>Экз</Text>
                </Col>
                <Col size={20} style={styles.justifyContentCenter}>
                  <Text style={styles.textPadding}>4В(87)</Text>
                </Col>
              </Row>
              <Row style={styles.titleBorder}>
                <Col size={60} style={styles.justifyContentCenter}>
                  <Text style={styles.textPadding}>
                    Управление IT-проектами
                  </Text>
                </Col>
                <Col size={20} style={styles.justifyContentCenter}>
                  <Text style={styles.textPadding}>Экз</Text>
                </Col>
                <Col size={20} style={styles.justifyContentCenter}>
                  <Text style={styles.textPadding}>5A(96)</Text>
                </Col>
              </Row>
              <Row style={styles.titleBorder}>
                <Col size={60} style={styles.justifyContentCenter}>
                  <Text style={styles.textPadding}>Веб-технологии</Text>
                </Col>
                <Col size={20} style={styles.justifyContentCenter}>
                  <Text style={styles.textPadding}>Зач</Text>
                </Col>
                <Col size={20} style={styles.justifyContentCenter}>
                  <Text style={styles.textPadding}>4C(80)</Text>
                </Col>
              </Row>
              <Row style={styles.titleBorder}>
                <Col size={60} style={styles.justifyContentCenter}>
                  <Text style={styles.textPadding}>
                    Програмное и лингвистическое обеспечение интеллектуальных
                    систем
                  </Text>
                </Col>
                <Col size={20} style={styles.justifyContentCenter}>
                  <Text style={styles.textPadding}>Зач</Text>
                </Col>
                <Col size={20} style={styles.justifyContentCenter}>
                  <Text style={styles.textPadding}>3D(70)</Text>
                </Col>
              </Row>
            </Grid>
          </Content>
        </Swiper>
      </Container>
    );
  }
}
