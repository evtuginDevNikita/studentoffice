import React, { Component } from "react";
import { Container, Content, Text, ListItem } from "native-base";
import Headers from "../common/Headers";

export default class Info extends Component {
  render() {
    return (
      <Container>
        <Headers
          onPress={() => this.props.navigation.toggleDrawer()}
          title="Информация"
        />
        <Content>
          <ListItem>
            <Text> Евтюгин Никита Евгеньевич </Text>
          </ListItem>
          <ListItem>
            <Text> Компьютерные и информационные технологии </Text>
          </ListItem>
          <ListItem>
            <Text> Системы искусственного интеллекта </Text>
          </ListItem>
          <ListItem>
            <Text> 4 курс (7 семетр) </Text>
          </ListItem>
          <ListItem>
            <Text> КИТ-45б </Text>
          </ListItem>
          <ListItem>
            <Text> Бакалавр </Text>
          </ListItem>
          <ListItem>
            <Text> Дневная форма </Text>
          </ListItem>
          <ListItem>
            <Text> Бюджет </Text>
          </ListItem>
        </Content>
      </Container>
    );
  }
}
