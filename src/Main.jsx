import React from "react";
import { Root } from "native-base";
import {
  createDrawerNavigator,
  createStackNavigator,
  createAppContainer
} from "react-navigation";

import Progress from "./Progress";
import Info from "./Info";
import Arrears from "./Arrears";
import Raiting from "./Raiting";

import SideBar from "./common/SideBar";

const Drawer = createDrawerNavigator(
  {
    Info: {
      screen: Info
    },
    Progress: {
      screen: Progress
    },
    Arrears: {
      screen: Arrears
    },
    Raiting: {
      screen: Raiting
    }
  },
  {
    initialRouteName: "Info",
    contentComponent: props => <SideBar {...props} />
  }
);

const AppNavigator = createStackNavigator(
  {
    Drawer: { screen: Drawer }
  },
  {
    initialRouteName: "Drawer",
    headerMode: "none"
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default () => (
  <Root>
    <AppContainer />
  </Root>
);
