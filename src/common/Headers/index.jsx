import React, { Component } from "react";
import { View } from "react-native";
import { Header, Title, Button, Left, Right, Body, Icon } from "native-base";

export default class Headers extends Component {
  render() {
    return (
      <View>
        <Header>
          <Left>
            <Button transparent onPress={this.props.onPress}>
              <Icon name="menu" />
            </Button>
          </Left>
          <Body>
            <Title textAlign="center"> {this.props.title} </Title>
          </Body>
          <Right />
        </Header>
      </View>
    );
  }
}
