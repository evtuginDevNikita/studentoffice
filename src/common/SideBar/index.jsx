import React, { Component } from "react";
import { Text, StyleSheet, Image, Dimensions } from "react-native";
import { Content, ListItem, Icon } from "native-base";

const { width } = Dimensions.get("window");

const styles = StyleSheet.create({
  logo: {
    height: 120,
    width: width
  },
  textPadding: {
    padding: 10
  }
});

export default class SideBar extends Component {
  render() {
    const logoHPI = require("../../../public/Images/khpi2.jpg");
    const { navigation } = this.props;
    const { navigate } = navigation;
    return (
      <Content style={{ backgroundColor: "white" }}>
        <Image source={logoHPI} style={styles.logo} />
        <ListItem iconLeft button onPress={() => navigate("Info")}>
          <Icon name="man" />
          <Text style={styles.textPadding}>Общая информация</Text>
        </ListItem>
        <ListItem iconLeft button onPress={() => navigate("Progress")}>
          <Icon name="dictionary" />
          <Text style={styles.textPadding}>Успеваемость</Text>
        </ListItem>
        <ListItem iconLeft button onPress={() => navigate("Raiting")}>
          <Icon name="paper" />
          <Text style={styles.textPadding}>Рейтинг</Text>
        </ListItem>
        <ListItem iconLeft button onPress={() => navigate("Arrears")}>
          <Icon name="alert" />
          <Text style={styles.textPadding}>Долги</Text>
        </ListItem>
        <ListItem iconLeft>
          <Icon name="info-with-circle" />
          <Text style={styles.textPadding}>О приложении</Text>
        </ListItem>
      </Content>
    );
  }
}
