import React, { Component } from "react";
import { StyleSheet } from "react-native";
import {
  Container,
  Left,
  Right,
  Content,
  Text,
  Grid,
  Col,
  Row,
  ListItem
} from "native-base";
import Headers from "../common/Headers/";

const styles = StyleSheet.create({
  textPadding: {
    padding: 10
  },
  titleBorder: {
    borderBottomWidth: 1,
    marginHorizontal: 5,
    borderBottomColor: "grey"
  },
  contentCenter: {
    alignContent: "center",
    justifyContent: "center"
  },
  titleBackground: {
    justifyContent: "center",
    alignContent: "center",
    backgroundColor: "#3D51B4",
    color: "white"
  },
  textColor: {
    color: "white",
    padding: 10
  }
});

export default class Raiting extends Component {
  render() {
    return (
      <Container>
        <Headers
          onPress={() => this.props.navigation.toggleDrawer()}
          title="Рейтинг"
        />
        <Content>
          <ListItem>
            <Left>
              <Text>Ваша позиция в рейтинге</Text>
            </Left>
            <Right>
              <Text>5</Text>
            </Right>
          </ListItem>
          <ListItem>
            <Left>
              <Text>Средний балл</Text>
            </Left>
            <Right>
              <Text>91.9</Text>
            </Right>
          </ListItem>
          <Grid>
            <Row style={styles.titleBackground}>
              <Text style={styles.textColor}>Общий рейтинг</Text>
            </Row>
            {/* ///////////////////////////////////////////////////////////////////////////Table */}
            <Row style={styles.titleBorder}>
              <Col size={20} style={styles.contentCenter}>
                <Text style={styles.textPadding}>№</Text>
              </Col>
              <Col size={60}>
                <Text style={styles.textPadding}>ФИО</Text>
              </Col>
              <Col size={20} style={styles.contentCenter}>
                <Text>Балл</Text>
              </Col>
            </Row>
            <Row>
              <Col size={20} style={styles.contentCenter}>
                <Text style={styles.textPadding}>1</Text>
              </Col>
              <Col size={60}>
                <Text style={styles.textPadding}>Аюпов Р.</Text>
              </Col>
              <Col size={20} style={styles.contentCenter}>
                <Text>99</Text>
              </Col>
            </Row>

            <Row>
              <Col size={20} style={styles.contentCenter}>
                <Text style={styles.textPadding}>2</Text>
              </Col>
              <Col size={60}>
                <Text style={styles.textPadding}>Бондаренко И.</Text>
              </Col>
              <Col size={20} style={styles.contentCenter}>
                <Text>97.6</Text>
              </Col>
            </Row>

            <Row>
              <Col size={20} style={styles.contentCenter}>
                <Text style={styles.textPadding}>3</Text>
              </Col>
              <Col size={60}>
                <Text style={styles.textPadding}>Боровик И.</Text>
              </Col>
              <Col size={20} style={styles.contentCenter}>
                <Text>97.4</Text>
              </Col>
            </Row>

            <Row>
              <Col size={20} style={styles.contentCenter}>
                <Text style={styles.textPadding}>4</Text>
              </Col>
              <Col size={60}>
                <Text style={styles.textPadding}>Горянский Д.</Text>
              </Col>
              <Col size={20} style={styles.contentCenter}>
                <Text>94</Text>
              </Col>
            </Row>

            <Row>
              <Col size={20} style={styles.contentCenter}>
                <Text style={styles.textPadding}>5</Text>
              </Col>
              <Col size={60}>
                <Text style={styles.textPadding}>Евтюгин Н.</Text>
              </Col>
              <Col size={20} style={styles.contentCenter}>
                <Text>91.9</Text>
              </Col>
            </Row>

            <Row>
              <Col size={20} style={styles.contentCenter}>
                <Text style={styles.textPadding}>6</Text>
              </Col>
              <Col size={60}>
                <Text style={styles.textPadding}>Жушман К.</Text>
              </Col>
              <Col size={20} style={styles.contentCenter}>
                <Text>89</Text>
              </Col>
            </Row>

            <Row>
              <Col size={20} style={styles.contentCenter}>
                <Text style={styles.textPadding}>7</Text>
              </Col>
              <Col size={60}>
                <Text style={styles.textPadding}>Зиньковский А.</Text>
              </Col>
              <Col size={20} style={styles.contentCenter}>
                <Text>87</Text>
              </Col>
            </Row>

            <Row>
              <Col size={20} style={styles.contentCenter}>
                <Text style={styles.textPadding}>8</Text>
              </Col>
              <Col size={60}>
                <Text style={styles.textPadding}>Кобзина</Text>
              </Col>
              <Col size={20} style={styles.contentCenter}>
                <Text>85.4</Text>
              </Col>
            </Row>

            <Row>
              <Col size={20} style={styles.contentCenter}>
                <Text style={styles.textPadding}>9</Text>
              </Col>
              <Col size={60}>
                <Text style={styles.textPadding}>Ковтун К.</Text>
              </Col>
              <Col size={20} style={styles.contentCenter}>
                <Text>81</Text>
              </Col>
            </Row>

            <Row>
              <Col size={20} style={styles.contentCenter}>
                <Text style={styles.textPadding}>10</Text>
              </Col>
              <Col size={60}>
                <Text style={styles.textPadding}>Кухар А.</Text>
              </Col>
              <Col size={20} style={styles.contentCenter}>
                <Text>78</Text>
              </Col>
            </Row>

            <Row>
              <Col size={20} style={styles.contentCenter}>
                <Text style={styles.textPadding}>11</Text>
              </Col>
              <Col size={60}>
                <Text style={styles.textPadding}>Ляднова К</Text>
              </Col>
              <Col size={20} style={styles.contentCenter}>
                <Text>76</Text>
              </Col>
            </Row>
          </Grid>
        </Content>
      </Container>
    );
  }
}
