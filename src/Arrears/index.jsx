import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { Container, Left, Right, Content, Text, ListItem } from "native-base";
import Headers from "../common/Headers";

const styles = StyleSheet.create({
  titleBorder: {
    borderBottomWidth: 1,
    marginHorizontal: 5,
    borderBottomColor: "grey"
  }
});

export default class Arrears extends Component {
  render() {
    return (
      <Container>
        <Headers
          onPress={() => this.props.navigation.toggleDrawer()}
          title="Долги"
        />
        <Content>
          <ListItem style={styles.titleBorder}>
            <Left>
              <Text>Предмет</Text>
            </Left>
            <Right>
              <Text>Семестр</Text>
            </Right>
          </ListItem>
          {/* ///////////////////////////////////////////////////////////////////////////Долги */}
          <ListItem>
            <Left>
              <Text>Теория алгоритмов</Text>
            </Left>
            <Right>
              <Text>6</Text>
            </Right>
          </ListItem>
          <ListItem>
            <Left>
              <Text>Интеллектуальный анализ данных</Text>
            </Left>
            <Right>
              <Text>6</Text>
            </Right>
          </ListItem>
          <ListItem>
            <Left>
              <Text>Украинский язык</Text>
            </Left>
            <Right>
              <Text>4</Text>
            </Right>
          </ListItem>
        </Content>
      </Container>
    );
  }
}
